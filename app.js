const html = require('choo/html');
const choo = require('choo');
const moment = require('moment');
const logoGitlab = require('./gitlab-logo.js');

const app = choo();
app.route('/', mainView);
app.route('/kah-roo-lee', mainView);
app.mount('body');

let viewport = document.createElement('meta');
viewport.name = 'viewport';
viewport.content = 'width=device-width';
document.getElementsByTagName('head')[0].appendChild(viewport);

let mwac = document.createElement('meta');
mwac.name = 'mobile-web-app-capable';
mwac.content = 'yes';
document.getElementsByTagName('head')[0].appendChild(mwac);

let linkmeta = document.createElement('link');
linkmeta.rel = 'icon';
linkmeta.sizes = '192x192';
linkmeta.href = './logo.png';
document.getElementsByTagName('head')[0].appendChild(linkmeta);

var cm_min;
var meters;
var value = 0;
var time = 0;
var date;

function mainView(state, emit) {
  emit('DOMTitleChange', 'karouli');
  return html`
    <body>
	    <style>
				body {
					background-color: rgb(45, 45, 45);
					font-family: monospace;
					overflow: hidden;
          width: 100%;
          margin: 0;
				}
				header {
					text-align: center;
				}
				header h1 {
					color: #50e3c2;
					font-size: 7vh;
				}
				.container {
					margin: 50px auto;
					text-align: center;
				}
				h1, h2, footer a {
					color: #dc5c60;
				}
				.container h1 {
					font-size: 6vh;
					margin-bottom: 5px;
				}
				.container h2 {
					margin-top: 5px;
					font-size: 3vh;
				}
				form {
					padding-top: 5vh;
				}
				#calculate {
					display: inline-block;
				}
				input {
					display: block;
					font-size: 2vh;
					margin-top: 20px;
					padding: 12px 27px;
					border: none;
				}
				input[type=number]::-webkit-inner-spin-button,
				input[type=number]::-webkit-outer-spin-button {
    			-webkit-appearance: none;
    			-moz-appearance: none;
    			appearance: none;
    			margin: 0;
				}
				footer {
					color: #50e3c2;
					text-align: center;
					padding-top: 3vh;
					padding-bottom: 3vh;
					font-size: 2vh;
				}
        footer p {
          display: flex;
          align-items: center;
          justify-content: center;
          white-space: pre-wrap;
        }
			</style>
			<header>
        <h1>[/kah-roo-lee/]</h1>
			</header>
			<div class="container">
				<h1>${time}</h1>
				<h2>${date}</h2>
				<form id="calculate">
        	<input oninput=${oninput} value="${cm_min}" placeholder="cm/min" name="cm_min" type="number" required>
					<input oninput=${oninput} value="${meters}" placeholder="meters" name="meters" type="number" required>
      	</form>
				</div>
				<footer>
					<p>
						<a href="https://www.gitlab.com/pragalakis/kah-roo-lee" style="text-decoration: none;" target="_blank">
            ${logoGitlab}
						</a>
					 with <a href="https://choo.io/" target="_blank">choo</a> & js </p>
    	</footer>
    </body>
  `;

  function oninput(e) {
    e.preventDefault();
    var currentDate = new Date();

    if (e.target.name === 'cm_min') {
      if (e.target.value > 10000) {
        cm_min = 10000;
      } else {
        cm_min = e.target.value;
      }
    } else if (e.target.name === 'meters') {
      if (e.target.value > 10000) {
        meters = 10000;
      } else {
        meters = e.target.value;
      }
    }

    minutes = Number(100 / cm_min) * Number(meters);
    result = moment(currentDate).add(minutes, 'm');
    date = result.format('dddd D MMMM Y');
    time = result.format('HH:mm');
    emit('render');
  }
}
